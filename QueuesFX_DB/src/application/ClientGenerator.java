package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

public class ClientGenerator implements Runnable{
	
	private final static Logger LOGGER = Logger.getLogger(ClientGenerator.class.getName()); 

	private int numberOfClients = 100;
	
	private static int currentTime = 0;
	
	//data read from UI
	private static int timeLimit = 10;
	private int maxIntervalArrivingTime;
	private int minIntervalArrivingime;
	private int maxProcessingTime;
	private int minProcessingTime;
	private static int maxStatisticsTime;
	private static int minStatisticsTime;
	
	private int noOfQueues;
	private volatile int id = 1;
	
	private Shop shop;
	private List<Client> generatedClients;
	
	//peak time
	private int peakTime = 0;
	private int largestQueuesSize = 0;
	
	public ClientGenerator(int maxIntervalArrivingTime, int minIntervalArrivingTime, int maxProcessingTime, int minProcessingTime, int noOfQueues, int maxStatisticsTime, int minStatisticsTime){
		this.maxIntervalArrivingTime = maxIntervalArrivingTime;
		this.minIntervalArrivingime = minIntervalArrivingTime;
		this.maxProcessingTime = maxProcessingTime;
		this.minProcessingTime = minProcessingTime;
		this.noOfQueues = noOfQueues;
		ClientGenerator.maxStatisticsTime = maxStatisticsTime;
		ClientGenerator.minStatisticsTime = minStatisticsTime;
		shop = new Shop(noOfQueues);
		generatedClients = new ArrayList<Client>();
		generateNRandomClients();
	}
	
	public ClientGenerator(){
		this(5, 1, 10, 3, 5, 5, 1);
	}
	
	private void generateNRandomClients(){
		
		int arrivingTime, processingTime;
		Random rand = new Random();
		for(int i = 0; i < numberOfClients; i++){
			id++;
			arrivingTime = rand.nextInt(getTimeLimit()) + 1;
			processingTime = rand.nextInt((maxProcessingTime - minProcessingTime) + 1) + minProcessingTime;
			generatedClients.add(new Client(id, arrivingTime, processingTime));
		}
		generatedClients.sort(null);
		
		String s = new String("clientii generati: \n");
		for(Client client : generatedClients){
			s = s + client.toString();
		}		
		//System.out.println(s);
	}
	
	public ShopQueue bestQueueToAddClient(){		
		ShopQueue qFound = shop.getDesks().get(0);
		int minim = qFound.getWaitingPeriod().intValue();
		for(ShopQueue q : shop.getDesks()){
			if(q.getWaitingPeriod().intValue() < minim){
				minim = q.getWaitingPeriod().intValue();
				qFound = q;
			}
		}
		return qFound;
	}
	
	public int getFirstArrivalTime(){
		if(!generatedClients.isEmpty()){
			return generatedClients.get(0).getArrivalTime();
		}
		return 0;
	}
	
	public void run(){
		int arrivingTime = 0;
		Random rand = new Random();
		while(currentTime < getTimeLimit()){
			LOGGER.info("current time: " + currentTime);
			ShopQueue q = bestQueueToAddClient();
			arrivingTime = rand.nextInt((maxIntervalArrivingTime - minIntervalArrivingime) + 1) + minIntervalArrivingime;
			try {				
				Thread.sleep(arrivingTime * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				if( (!generatedClients.isEmpty())){
					generatedClients.get(0).setArrivalTime(arrivingTime);
					q.addClient(generatedClients.get(0));
					LOGGER.info("clientul cu id:" + generatedClients.get(0).getID() + " se pune la coada cu crt: " + q.getCrt());
					generatedClients.remove(0);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			LOGGER.info( "\n" + shop.toString());
			findPeakTime(currentTime);
			currentTime++;
		}
		LOGGER.info("PeakTime = " + peakTime);
	}
	
	public void findPeakTime(int currentTime){//when the sum of the size of the queues is the greatest
		int currentQueuesSize = 0;
		for(ShopQueue sq: shop.getDesks()){
			currentQueuesSize += sq.getQueue().size();
		}
		if(currentQueuesSize > largestQueuesSize){
			largestQueuesSize = currentQueuesSize;
			peakTime = currentTime;
		}			
	}
	
	public Shop getShop(){
		return shop;
	}

	public int getNoOfQueues() {
		return noOfQueues;
	}

	public void setNoOfQueues(int noOfQueues) {
		this.noOfQueues = noOfQueues;
	}

	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}

	public void setMaxProcessingTime(int maxProcessingTime) {
		this.maxProcessingTime = maxProcessingTime;
	}

	public int getMinProcessingTime() {
		return minProcessingTime;
	}

	public void setMinProcessingTime(int minProcessingTime) {
		this.minProcessingTime = minProcessingTime;
	}

	public static int getTimeLimit() {
		return timeLimit;
	}

	public static void setTimeLimit(int timeLimit) {
		ClientGenerator.timeLimit = timeLimit;
	}

	public static int getCurrentTime() {
		return currentTime;
	}

	public static void setCurrentTime(int currentTime) {
		ClientGenerator.currentTime = currentTime;
	}

	public static int getMaxStatisticsTime() {
		return maxStatisticsTime;
	}

	public void setMaxStatisticsTime(int maxStatisticsTime) {
		ClientGenerator.maxStatisticsTime = maxStatisticsTime;
	}

	public static int getMinStatisticsTime() {
		return minStatisticsTime;
	}

	public void setMinStatisticsTime(int minStatisticsTime) {
		ClientGenerator.minStatisticsTime = minStatisticsTime;
	}

	public int getPeakTime() {
		return peakTime;
	}

	public void setPeakTime(int peakTime) {
		this.peakTime = peakTime;
	}
}
